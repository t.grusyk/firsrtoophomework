package animal;

abstract public class Fish extends Animal{
    private double paws;

    public void setPawsFish(double paws) {
        this.paws = paws;
    }

    public double getPawsFish() {
        return paws;
    }
    public void fishInfo(){
        System.out.println("Живе під водою бла бла бла...");
    }
    abstract public void sharkInfo();

}
