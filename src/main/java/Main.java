import animal.*;

public class Main {
    public static void main(String[] args) {
        Animal animal = new Animal();
        Hawk hawk = new Hawk();
        Cat cat = new Cat();
        Racoon racoon  = new Racoon();
        Fish shark = new Shark();
        Earth earth = new Earth();

        System.out.println("\tCat");
        cat.setPawsCatRacoon(4);
        cat.printInfo("Cat");

        System.out.println("\tHawk");
        hawk.setPawsHawk(2);
       //hawk.printInfoHawk();
        hawk.printInfo("Hawk");

        System.out.println("\tRacoon");
        racoon.setPawsCatRacoon(4);
        racoon.printInfo("Racoon");

        System.out.println("\tFish");
        shark.fishInfo();
        shark.sharkInfo();
    }
}
